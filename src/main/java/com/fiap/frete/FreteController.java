package com.fiap.frete;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FreteController {
    @Autowired
    FreteService freteService;

    @GetMapping("/frete")
    private List<Frete> getAllViews() {
        return freteService.getAllViews();
    }

    @GetMapping("/frete/{id}")
    private Frete getView(@PathVariable("id") int id) {
        return freteService.getViewById(id);
    }

    @DeleteMapping("/frete/{id}")
    private void deleteView(@PathVariable("id") int id) {
      freteService.delete(id);
    }

    @PostMapping("/frete")
    private int saveView(@RequestBody Frete frete) {
      freteService.saveOrUpdate(frete);
        return frete.getId();
    }
}