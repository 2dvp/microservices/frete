package com.fiap.frete;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DESEJOS")
public class Frete {
    @Id
    @GeneratedValue
    public int id;
    public int tare_price; 
    public int tare_size;
    public String user_addrr;
    public Integer getId() {
       return id;
   } 
}