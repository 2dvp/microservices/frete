package com.fiap.frete;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FreteService {
    @Autowired
    FreteRepository freteRepository;

    public List<Frete> getAllViews() {
        List<Frete> fretes = new ArrayList<Frete>();
        freteRepository.findAll().forEach(frete -> fretes.add(frete));
        return fretes;
    }

    public Frete getViewById(int id) {
        return freteRepository.findById(id).get();
    }

    public void saveOrUpdate(Frete view) {
        freteRepository.save(view);
    }


    public void delete(int id) {
        freteRepository.deleteById(id);
    }    
}