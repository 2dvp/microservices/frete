package com.fiap.frete;

import org.springframework.data.repository.CrudRepository;

public interface FreteRepository extends CrudRepository<Frete, Integer> {
    
}